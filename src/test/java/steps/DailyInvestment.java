package steps;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

import capability.DateAndTimeUtility;

public class DailyInvestment
{
	CSVWriter writer;
	List<String[]> data;
	WebDriver driver;
	DateAndTimeUtility setTime=new DateAndTimeUtility();
	String Data[]= {"Subject","count"};
	String Row[]= {"DailyInvestment",""};
	//String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTI1LCJmaWx0ZXIiOlsiYW5kIixbImJldHdlZW4iLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDExNTddLCJtaW51dGUiXSwiMjAyMi0wNC0wNlQwNjowMDowMCIsIjIwMjItMDQtMDZUMDg6MDA6MDAiXV0sImFnZ3JlZ2F0aW9uIjpbWyJkaXN0aW5jdCIsWyJmaWVsZC1pZCIsMTEyM11dXX0sImRhdGFiYXNlIjoyfSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTI1LCJmaWx0ZXIiOlsiYW5kIixbImJldHdlZW4iLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDExNTddLCJtaW51dGUiXSwiMjAyMi0wNC0wNlQwNjowMDowMCIsIjIwMjItMDQtMDZUMDg6MDA6MDAiXV0sImFnZ3JlZ2F0aW9uIjpbWyJkaXN0aW5jdCIsWyJmaWVsZC1pZCIsMTEyM11dXX0sImRhdGFiYXNlIjoyfSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	public DailyInvestment(WebDriver driver, CSVWriter writer,List<String[]> data) throws Exception
	{
		this.driver=driver;
		this.data=data;
		driver.get(URL);
		Thread.sleep(5000);
		setTime.setCurrentDateWithTime(driver);
		Row[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
        Thread.sleep(5000);
		writer.writeNext(Data);
		writer.writeNext(Row);
		data.add(new String[] {"","All data on "+setTime.getCurrentDate()+" till at "+setTime.getCurrentTime()});
		data.add(new String[] {"MonitorName","Count",""});
		writer.flush();


	}
}
