package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.opencsv.CSVWriter;

import capability.Capability;
import capability.ExcelSheet;
import steps.DailyInvestment;
import steps.Login;

public class MainClass extends Capability
{
	public List<String[]> data=new ArrayList<String[]>();
	CSVWriter writer;
	ExcelSheet excelFile=new ExcelSheet();
	WebDriver driver;
	@BeforeTest
	public void openchrome() throws Exception
	{
		this.driver = Capability();
		this.writer=excelFile.ExcelSheet();
		driver.manage().window().maximize();
	}
	
	@Test(priority = 1)
	public void login() throws InterruptedException
	{
		new Login(driver);
	}
	@Test(priority = 2)
	public void investment() throws Exception
	{
		new DailyInvestment(driver, writer, data);
	}
	@AfterTest
	public void CloseBrowser()
	{

		driver.quit	();
	}

}
